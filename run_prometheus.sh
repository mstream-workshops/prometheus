echo "building image..."
docker build -t mstream/prometheus:v1.0.0 ./prometheus 1>/dev/null
container_id=$(docker run -d -P --name prometheus --net prometheus mstream/prometheus:v1.0.0)
echo -n "prometheus ${1} ${container_id} => "
docker inspect "${container_id}" | grep HostPort | sed -E 's/.*"HostPort": "(.*)"/http:\/\/localhost:\1/'
