echo "building image..."
docker build -t mstream/producer:v1.0.0 ./producer 1>/dev/null
container_id=$(docker run -d -P --name "producer${1}" --net prometheus mstream/producer:v1.0.0)
echo -n "producer ${1} ${container_id} => "
docker inspect "${container_id}" | grep HostPort | sed -E 's/.*"HostPort": "(.*)"/http:\/\/localhost:\1\/metrics.txt/'
